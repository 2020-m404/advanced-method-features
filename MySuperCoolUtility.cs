﻿using System;
using System.Linq;

namespace AdvancedMethods
{
    class MySuperCoolUtility
    {
        /// <summary>
        /// Just a simple method which returns the square value of the given parameter value.
        /// The implementation reassigns the result to the parameter variable intentionally.
        /// You'll notice, albeit the mutation, the original arguments value outside of this
        /// method will not be affected by the calculations.
        /// </summary>
        /// <param name="a">Input value to calculate the square of.</param>
        /// <returns>Square value of input parameter a.</returns>
        public int Square(int a)
        {
            a *= a;
            return a;
        }

        /// <summary>
        /// Since the methods parameter gets passed by reference, there's no need for a return value.
        /// Note: This is a rather uncommon usage of method parameters.
        /// More often, you'll see this pattern in functional programming (out of scope for M404).
        /// </summary>
        /// <param name="x">Input value which gets squared by reference.</param>
        public void SquareByReference(ref int x)
        {
            x *= x;
        }

        /// <summary>
        /// Since the parameter values is passed by reference,
        /// mutations to its values are reflected in the calling scope.
        /// </summary>
        /// <param name="values">An array of integer values in which a value should be replaced.</param>
        /// <param name="position">The position, at which the replacement should take place.</param>
        /// <param name="newValue">The new value to be inserted in the array.</param>
        public void ReplaceA(int[] values, int position, int newValue)
        {
            values[position] = newValue;
            Console.WriteLine($"Value of element at position [{position}] inside ReplaceA method: {values[position]}");
        }

        /// <summary>
        /// Again, the parameter values is passed by reference.
        /// On the second line although, a new instance is assigned to the parameter.
        /// This reassignment to a different memory location has only local scope!
        /// </summary>
        /// <param name="values">An array of integer values in which a value should be replaced.</param>
        /// <param name="position">The position, at which the replacement should take place.</param>
        /// <param name="newValue">The new value to be inserted in the array.</param>
        public void ReplaceB(int[] values, int position, int newValue)
        {
            values[position] = newValue;
            // Generate array containing a range of numbers, starting with 100
            values = Enumerable.Range(100, position + 1).ToArray();
            Console.WriteLine($"Value of element at position [{position}] inside ReplaceB method: {values[position]}");
        }

        /// <summary>
        /// Again, the parameter values is passed by reference.
        /// The only difference to the above method ReplaceB is the presence of the ref keyword.
        /// </summary>
        /// <param name="values">An array of integer values in which a value should be replaced.</param>
        /// <param name="position">The position, at which the replacement should take place.</param>
        /// <param name="newValue">The new value to be inserted in the array.</param>
        public void ReplaceC(ref int[] values, int position, int newValue)
        {
            values[position] = newValue;
            // Generate array containing a range of numbers, starting with 100
            values = Enumerable.Range(100, position + 1).ToArray();
            Console.WriteLine($"Value of element at position [{position}] inside ReplaceC method: {values[position]}");
        }

        /// <summary>
        /// This method performs a quite primitive check whether the given email address is formally valid.
        /// Actually, it only verifies if there's at least one @ character in between other characters. 
        /// The result of the check (boolean) is returned.
        /// As a side product, the method extracts the email addresses user and domain portion
        /// which are assigned to the corresponding out parameters.
        /// </summary>
        /// <param name="email">The input email address to be validated.</param>
        /// <param name="user">Output parameter for the user portion of the email address.</param>
        /// <param name="domain">Output parameter for the domain portion of the email address.</param>
        /// <returns>TRUE if the given email address is formally valid, FALSE otherwise.</returns>
        public bool IsValidEmail(string email, out string user, out string domain)
        {
            string[] emailParts = email.Split('@');

            // Formal validation of email address
            if (emailParts.Length == 2
                && !string.IsNullOrWhiteSpace(emailParts[0])
                && !string.IsNullOrWhiteSpace(emailParts[1]))
            {
                // Assign user and domain portion of formally valid email address
                // to its corresponding out parameters.
                user = emailParts[0];
                domain = emailParts[1];

                return true;
            }

            // To every out parameter, a value must be assigned!
            // (Try to remove the following lines to verify!)
            user = null;
            domain = null;

            return false;
        }

        /// <summary>
        /// The method returns the week day (as a string) of the given date.
        /// There are default values assigned to each parameter.
        /// This allows for method calls specifying only some of the parameters.
        /// </summary>
        /// <param name="day">The day of the month. Default value: 1.</param>
        /// <param name="month">The month of the year. Default value: 1 (january).</param>
        /// <param name="year">The year. Default value: 2020.</param>
        /// <returns></returns>
        public string GetDayOfWeek(int day = 1, int month = 1, int year = 2020)
        {
            return new DateTime(year, month, day).DayOfWeek.ToString().ToUpper();
        }
    }
}