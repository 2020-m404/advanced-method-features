﻿using System;

namespace AdvancedMethods
{
    class Program
    {
        static void Main(string[] args)
        {
            /*
             * Initialize utility object
             */
            MySuperCoolUtility utility = new MySuperCoolUtility();


            /*
             * Simple values are passed BY VALUE (copy)
             */
            int a = 10;
            int squareOfA = utility.Square(a);
            // Parameter a is not affected by calculations in method
            Console.WriteLine($"Square of {a} = {squareOfA}\n");


            /*
             * Value types may be passed BY REFERENCE using the REF keyword
             */
            int b = 10;
            int copyOfB = b; // Only kept for output (value types are actually copied)
            utility.SquareByReference(ref b); // Pass by reference using the ref keyword
            Console.WriteLine($"Square of {copyOfB} = {b}\n");


            /*
             * Reference type values are passed BY REFERENCE
             */
            int[] valuesA = new int[] {1, 2, 3};
            int position = 2;
            Console.WriteLine($"Value of element at position [{position}] before method call:     {valuesA[position]}");
            utility.ReplaceA(valuesA, position, 4);
            Console.WriteLine(
                $"Value of element at position [{position}] after method call:      {valuesA[position]}\n");

            /*
             * Reference type values are passed BY REFERENCE
             * (by default; without usage of ref keyword)
             */
            int[] valuesB = new int[] {1, 2, 3};
            Console.WriteLine($"Value of element at position [{position}] before method call:     {valuesB[position]}");
            utility.ReplaceB(valuesB, 2, 4);
            Console.WriteLine(
                $"Value of element at position [{position}] after method call:      {valuesB[position]}\n");

            /*
             * Reference type values are passed BY REFERENCE
             * (with usage of ref keyword)
             */
            int[] valuesC = new int[] {1, 2, 3};
            Console.WriteLine($"Value of element at position [{position}] before method call:     {valuesC[position]}");
            utility.ReplaceC(ref valuesC, 2, 4);
            Console.WriteLine(
                $"Value of element at position [{position}] after method call:      {valuesC[position]}\n");


            /*
             * Usage of the OUT keyword for multiple return values
             */
            string[] emailAddresses = new string[]
            {
                "peter.gisler@gibz.ch",
                "peter.gisler_gibz.ch",
                "peter.gisler",
                "gibz.ch",
            };

            foreach (string email in emailAddresses)
            {
                string user, domain;
                Console.WriteLine(utility.IsValidEmail(email, out user, out domain)
                    ? $"'{user}' is part of '{domain}'"
                    : $"'{email}' is formally an invalid email address."
                );
            }


            /*
             * Default parameter values.
             * If there are default values assigned to parameters,
             * you may omit arguments at the end of the parameter list.
             */
            Console.WriteLine($"\nIn 2020, Christmas is on {utility.GetDayOfWeek(25, 12, 2020)}.");
            Console.WriteLine($"In 2020, Swiss national holiday was on {utility.GetDayOfWeek(1, 8)}.");
            Console.WriteLine($"2020 started with a {utility.GetDayOfWeek()}.");


            /*
             * With named arguments, order of parameters is irrelevant.
             * Now you may omit any arguments which have default values assigned.
             */
            Console.WriteLine($"\nIn 2020, New Year's Eve is on {utility.GetDayOfWeek(month: 12, day: 31)}.");
            Console.WriteLine($"One day later, it will be a {utility.GetDayOfWeek(year: 2021)}.");
        }
    }
}